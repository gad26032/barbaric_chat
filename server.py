import time
import threading
import select
from socketserver import TCPServer, UDPServer, ThreadingMixIn
from server_pefs import *
from socketserver import StreamRequestHandler, DatagramRequestHandler, BaseRequestHandler
from utils import MsgManager
from protocol.core import MsgUser, MsgGetContacts, MsgPresence, MsgAuthenticate

import datetime

CONNECTIONS = []


class TcpServer(TCPServer):
    allow_reuse_address = True
    timeout = 120

    def shutdown_request(self, request):
        print('shutdown_request')

    def close_request(self, request):
        print('close_request')


class TCPConnectionHandler(BaseRequestHandler):
    def handle(self):
        msg = manager.msg_decode(self.request.recv(1024)[:-1])
        print(msg)
        m = manager.determinate_msg(msg)
        if isinstance(m, MsgUser):
            print('{} - {} to {} : {} '.format(m.time, m.from_, m.to, m.message))
        elif isinstance(m, MsgGetContacts):
            print('отправить контакты пользователю')
        elif isinstance(m, MsgAuthenticate):
            print('Авторизован')
            if m.status == 'receive':
                server.receivers[m.user] = self.request
                # print(server.receivers)
                print(len(server.receivers))
            elif m.status == 'send':
                server.senders.append(self.request)
                print(server.senders)
                # print(len(server.senders))

    # def finish(self):
    #     print('finish')


# class ThreadedTCPRequestHandler(BaseRequestHandler):
#     def handle(self):
#         msg = manager.msg_decode(self.request.recv(1024))
#         print(msg)
#         m = manager.determinate_msg(msg)
#         if isinstance(m, MsgUser):
#             print('{} - {} to {} : {} '.format(m.time, m.from_, m.to, m.message))
#             if m.to == '#all':
#                 time = datetime.datetime.now()
#                 msg = manager.dictify(m)
#                 msg = manager.msg_encode(msg)
#                 self.request.sendall(msg)
#
#         elif isinstance(m, MsgGetContacts):
#             print('отправить контакты пользователю')
#
#
# class ThreadedTCPServer(ThreadingMixIn, TcpServer):
#     pass


class ChatServer:
    def __init__(self, addr, port, quantity, receivers, senders, msgs):
        self.addr = addr
        self.port = port
        self.quantity = quantity
        self.service = self.serve = TcpServer(('', self.port), TCPConnectionHandler)
        self.receivers = receivers
        self.senders = senders
        self.msgs = msgs
        # self.service = self.serve = TcpServer(('', self.port), TCPConnectionHandler)

    def start_serve(self):
        print('Server started!')
        self.service.serve_forever()

    def server_info(self):
        print('Сервер будет запущен со следующими параметрами \n'
              'ip_addr: {}\n'
              'port: {}\n'
              'max_connections{}\n'.format(self.addr, self.port, self.quantity))

        # def start_server(self, ):
        #     self.server_info()

    def msg_receiver(self):
        while True:
            while self.senders:
                # print(self.receivers)
                r, w, x = select.select(self.senders, [], [], 1)
                if r:
                    for conn in r:
                        msg = conn.recv(4048)
                        print(msg)
                        try:
                            for m in msg.split(b'\x00'):
                                if m:
                                    m = manager.msg_decode(m)
                                    m = manager.determinate_msg(m)
                                    if isinstance(m, MsgUser):
                                        print('{} - {} to {} : {} '.format(m.time, m.from_, m.to, m.message))
                                        self.msgs.append(m)
                        except Exception as e:
                            print(e)
                    time.sleep(1)
            time.sleep(2)

    def msg_sender(self):
        while True:
            while self.receivers:
                # print(self.receivers)
                # r, w, x = select.select([], self.receivers, [], 1)
                for m in list(self.msgs):
                    # print(m.to)
                    conn = self.receivers.get(m.to)
                    if conn:
                        msg = manager.dictify(m)
                        msg = manager.msg_encode(msg)
                        conn.send(msg + b'\0')
                        self.msgs.remove(m)
                time.sleep(1)
            time.sleep(2)


if __name__ == '__main__':
    rcvs = dict()
    send = []
    messages = []

    manager = MsgManager()

    # s = Server
    server = ChatServer(ADDR, PORT, 5, rcvs, send, messages)

    server_thread = threading.Thread(target=server.start_serve)
    listen_thread = threading.Thread(target=server.msg_receiver)
    send_thread = threading.Thread(target=server.msg_sender)

    server_thread.start()
    listen_thread.start()
    send_thread.start()

    server_thread.join()
    listen_thread.join()
    send_thread.join()



