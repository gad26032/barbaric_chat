# coding: utf-8
# -*- coding: utf-8 -*-
from socket import *
import json
import time
import datetime
from multiprocessing import Process
import threading
import sys
import getopt
from utils import MsgManager
from protocol.core import User
from protocol.core import MsgUser, MsgPresence, MsgGetContacts, MsgAuthenticate


def send_msg(msg, conn):
    conn.send(msg + b'\0')


def msg_receiver(conn):
    while True:
        try:
            msg = conn.recv(4048)
            if msg:
                for m in msg.split(b'\x00'):
                    if m:
                        m = manager.msg_decode(m)
                        m = manager.determinate_msg(m)
                        if isinstance(m, MsgUser):
                            print('{} - {} to {} : {} '.format(m.time, m.from_, m.to, m.message))
                        else:
                            print('sys message: {}'.format(manager.dictify(m)))
        except Exception as e:
            print(e)
        time.sleep(1)


def msg_sender(user, sock):
    sys.stdin = open("/dev/tty")
    while True:
        try:
            msg = input('enter msg: ')
            if msg == 'exit!':
                print('goodbye')
                break
            elif msg == 'contacts!':
                time = datetime.datetime.now()
                msg = MsgGetContacts(time)
                msg = manager.dictify(msg)
                msg = manager.msg_encode(msg)
                send_msg(msg, sock)
            else:
                time = datetime.datetime.now()
                msg = MsgUser(time=time, **{'from': user}, to=all_room, message=msg)
                msg = manager.dictify(msg)
                msg = manager.msg_encode(msg)
                send_msg(msg, sock)
        except Exception as e:
            print(e)


def client_info(addr, port):
    print('Попытка подключения к серверу \n'
          'ip_addr: {}\n'
          'port: {}\n'.format(addr, port))


if __name__ == '__main__':
    PORT = 7777
    ADDR = 'localhost'

    options, remainder = getopt.getopt(sys.argv[1:], 'a:p:', ['addr=', 'port='])
    for opt, arg in options:
        print(opt)
        if opt in ('-a', '--addr'):
            ADDR = arg
        if opt in ('-p', '--port'):
            PORT = int(arg)

    client_info(ADDR, PORT)

    if len(remainder) > 1:
        print('слишком много аргументов')

    user = User()
    all_room = User(name='#all', status='Общая комната')
    c_time = datetime.datetime.now()
    presence_msg = MsgPresence(c_time, user)
    manager = MsgManager()
    presence_msg = manager.dictify(presence_msg)
    presence_msg = manager.msg_encode(presence_msg)

    if 'send' in remainder:
        sock = socket(AF_INET, SOCK_STREAM)
        sock.connect((ADDR, PORT))
        auth_msg = MsgAuthenticate(time=c_time, user=user, status='send')
        auth_msg = manager.dictify(auth_msg)
        auth_msg = manager.msg_encode(auth_msg)
        send_msg(auth_msg, sock)
        # sock.close()
        sender_proc = Process(target=msg_sender, args=(user, sock))
        sender_proc.start()
        sender_proc.join()

    elif 'receive' in remainder:
        s = socket(AF_INET, SOCK_STREAM)
        s.connect((ADDR, PORT))
        room = input('enter to room: ')
        room = User(name='#' + room)
        auth_msg = MsgAuthenticate(time=c_time, user=room, status='receive')
        auth_msg = manager.dictify(auth_msg)
        auth_msg = manager.msg_encode(auth_msg)
        send_msg(auth_msg, s)
        msg_proc = Process(target=msg_receiver, args=(s,))
        msg_proc.start()
        msg_proc.join()

    else:
        print('По видимому вы ошиблись с аргументом, возможные аргументы \n'
              ' send : режим отправки сообщений \n'
              ' receive : режим приема сообщений \n')
