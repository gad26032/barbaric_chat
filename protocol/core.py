from protocol.config import *
import datetime
from models import User, BaseType, TimeType


# todo доделать что бы доставал нужного юзверя по имени и засовывыл в параметр

def get_user_from_base(username):
    """ возвращает пользователя или None"""
    return User()


class ActionType(BaseType):
    def __set__(self, instance, value):
        if value not in ACTIONS:
            raise AttributeError(
                'Ошибка протокола, не верный параметр action = {}, отсутствует в списке разрешенных ACTIONS'.format(
                    value))
        if len(value) > 15:
            raise AttributeError('Ошибка протокола, не верный параметр action, длинна больше 15')
        setattr(instance, self.name, value)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class DestinationNameType(BaseType):
    def __set__(self, instance, value):
        if 0 < len(value) < 26 and isinstance(value, str):
            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр name')

    def __str__(self):
        return self.name


class ResponseType(BaseType):
    def __set__(self, instance, value):
        if value not in RESPONSE_CODES:
            raise AttributeError('Ошибка протокола, не верный параметр action')
        setattr(instance, self.name, value)

    def __str__(self):
        return self.name


class MessageType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, str) and len(value) < 501:
            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр action')

    def __str__(self):
        return self.name


class UserType(BaseType):
    def __set__(self, instance, value):
        cur_user = get_user_from_base(value)
        if cur_user:
            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр User или None')

    def __str__(self):
        return self.name.name

    def __repr__(self):
        return self.name.name


class ChatNameType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, str) and len(value) < 26 and value.startswith('#'):

            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр имя комнаты чата')

    def __str__(self):
        return self.name


class FromToUserNameType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, User):

            setattr(instance, self.name, value.name)
        elif isinstance(value, str):
            setattr(instance, self.name, value)
        else:
            raise AttributeError(
                'Ошибка протокола, не верный параметр имя пользователя, должен быть объект пользователь')

    def __str__(self):
        return self.name


class MsgPresence:
    action = ActionType('action')
    time = TimeType('time')
    user = UserType('user')

    def __init__(self, time, user, action=PRESENCE, *args, **kwargs):
        self.action = action
        self.time = time
        self.type = 'status'
        self.user = user


class MsgProbe:
    action = ActionType('action')
    time = TimeType('time')

    def __init__(self, time, action=PROBE, *args, **kwargs):
        self.action = action
        self.time = time


class MsgAlert:
    response = ResponseType('response')
    time = TimeType('time')

    def __init__(self, time, response, alert='', *args, **kwargs):
        self.response = response
        self.time = time
        self.alert = alert


class MsgError:
    response = ResponseType('response')
    time = TimeType('time')

    def __init__(self, time, response, error='', *args, **kwargs):
        self.response = response
        self.time = time
        self.error = error


# class MsgChat:
#     action = ActionType('action')
#     time = TimeType('time')
#     from_ = UserType('from_')
#     to = ChatNameType('to')
#     message = MessageType('message')
#
#     def __init__(self, time, user, message, to='#all', action=MSG, *args, **kwargs):
#         self.action = action
#         self.time = time
#         self.status = 'status'
#         self.from_ = user
#         self.to = to
#         self.message = message


class MsgUser:
    action = ActionType('action')
    time = TimeType('time')
    from_ = FromToUserNameType('from_')
    to = FromToUserNameType('to')

    def __init__(self, action=MSG, *args, **kwargs):
        self.action = action
        self.time = kwargs['time']
        self.status = 'status'
        self.from_ = kwargs['from']
        self.to = kwargs['to']
        self.message = kwargs['message']


class MsgJoin:
    action = ActionType('action')
    time = TimeType('time')
    room = ChatNameType('room')

    def __init__(self, time, room, action=JOIN, *args, **kwargs):
        self.action = action
        self.time = time
        self.room = room


class MsgLeave:
    action = ActionType('action')
    time = TimeType('time')
    room = ChatNameType('room')

    def __init__(self, time, room, action=LEAVE, *args, **kwargs):
        self.action = action
        self.time = time
        self.room = room


class MsgAuthenticate:
    action = ActionType('action')
    time = TimeType('time')
    user = FromToUserNameType('user')

    def __init__(self, action=AUTH, *args, **kwargs):
        self.action = action
        self.time = kwargs.get('time')
        self.user = kwargs.get('user')
        self.status = kwargs.get('status')


class MsgQuit:
    action = ActionType('action')

    def __init__(self, action=QUIT, *args, **kwargs):
        self.action = action


class MsgGetContacts:
    action = ActionType('action')
    time = TimeType('time')

    def __init__(self, time, action=GET_CONTACTS, *args, **kwargs):
        self.action = action
        self.time = time


MSG_CLASSES = {PRESENCE: MsgPresence,
               MSG: MsgUser,
               QUIT: MsgQuit,
               JOIN: MsgJoin,
               LEAVE: MsgLeave,
               AUTH: MsgAuthenticate,
               PROBE: MsgProbe,
               GET_CONTACTS: MsgGetContacts}

RESPONSE_CLASSES = {BASIC_NOTICE: MsgAlert, OK: MsgAlert, ACCEPTED: MsgAlert, WRONG_REQUEST: MsgError,
                    SERVER_ERROR: MsgError}

# class Message:
#     action = ActionType('action')
#     time = TimeType('time')
#     name = DestinationNameType('name')
#     response = ResponseType('response')
#
#     def __init__(self, action, time, response, text, name):
#         self.action = action
#         self.time = time
#         self.response = response
#         self.name = name
#         self.msg = text


if __name__ == '__main__':
    u = User()
    t = datetime.datetime.timestamp(datetime.datetime.now())
    m = MsgPresence(t, u)

    # print(dictify(m))
