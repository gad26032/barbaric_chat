import datetime
from protocol.core import MSG_CLASSES, RESPONSE_CLASSES
import json


class MsgManager:
    @classmethod
    def determinate_msg(cls, msg_dict):
        if isinstance(msg_dict, dict):
            if msg_dict.get('action') and msg_dict.get('action') in MSG_CLASSES.keys():
                return MSG_CLASSES[msg_dict.get('action')](**msg_dict)
            elif msg_dict.get('response') and msg_dict.get('response') in RESPONSE_CLASSES.keys():
                return MSG_CLASSES[msg_dict.get('response')](**msg_dict)
            else:
                raise TypeError('determinate_msg получил данные в не верном формате')
        else:
            raise TypeError('determinate_msg получил не верные данные')

    @classmethod
    def dictify(cls, obj):
        """Возвращает красивый словарь на основе MsgКласса, в принципе даже на основе любого класа"""
        d = {}
        for k in obj.__dict__.keys():
            val = obj.__getattribute__(k)
            if isinstance(val, datetime.datetime):
                val = val.timestamp()
            if '__dict__' in val.__dir__():
                val = cls.dictify(val)
            if k.startswith('_'):
                k = k[1:]
            if k.endswith('_'):
                k = k[:-1]
            d[k] = val
        return d

    @classmethod
    def msg_decode(cls, byte_msg):
        # print(byte_msg)
        return json.loads(byte_msg.decode('utf-8'))

    @classmethod
    def msg_encode(cls, dict_msg):
        return json.dumps(dict_msg).encode('utf-8')
