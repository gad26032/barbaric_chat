import datetime
# import sqlite3
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import mapper
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


class BaseType:
    def __init__(self, name):
        self.name = '_' + name

    def __get__(self, instance, owner):
        return getattr(instance, self.name)


class TimeType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, int) or isinstance(value, float):
            setattr(instance, self.name, datetime.datetime.fromtimestamp(value))
        elif isinstance(value, datetime.datetime):
            setattr(instance, self.name, value)
        else:
            raise AttributeError(
                'Ошибка протокола, не верный параметр timestamp, неверный тип данных')

    def __str__(self):
        return self.name.strftime('%Y-%m-%d %H:%M:%S')

        # def __repr__(self):
        #     return self.name.strftime('%Y-%m-%d %H:%M:%S')


class UserNameType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, str) and len(value) < 26:

            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр имя пользователя')

    def __str__(self):
        return self.name


class UserStatusType(BaseType):
    def __set__(self, instance, value):
        if isinstance(value, str) and len(value) < 51:

            setattr(instance, self.name, value)
        else:
            raise AttributeError('Ошибка протокола, не верный параметр имя статус пользователя')

    def __str__(self):
        return self.name


class User:
    name = UserNameType('name')
    status = UserStatusType('status')

    def __init__(self, name='default', status='Привет! Я использую BarbaricChat'):
        self.name = name
        self.status = status
        # self.password = 'pass'

    def __str__(self):
        return self.name


class VisitDB:
    def __init__(self, time, ip, *args, **kwargs):
        self.time = time
        self.ip = ip


Base = declarative_base()

friends_table = Table('friends', Base.metadata,
                      Column('user_id', Integer, ForeignKey('users.id'), primary_key=True),
                      Column('friend_id', Integer, ForeignKey('users.id'), primary_key=True)
                      )


class UserDB(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    status = Column(String)
    password = Column(String)
    friends = relationship('UserDB', secondary=friends_table, primaryjoin=id==friends_table.c.user_id,
                           secondaryjoin=id==friends_table.c.friend_id,)

    def __init__(self, name, status='Привет! Я использую BarbaricChat', password='pass'):
        self.name = name
        self.status = status
        self.password = password

    def return_user_object(self):
        return User(name=self.name, status=self.status, )


engine = create_engine('sqlite:///mydb.sqlite')
Session = sessionmaker(bind=engine)
session = Session()


#
# class UserManager:
#
#     @classmethod
#     def user_convert_to_db(cls, user_object):
#         if isinstance(user_object)


# mapper(User, users_table)

if __name__ == '__main__':
    Base.metadata.create_all(engine)
